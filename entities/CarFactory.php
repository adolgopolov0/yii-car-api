<?php

declare(strict_types=1);

namespace app\entities;

use app\models\Car as CarAR;

final class CarFactory
{

    public static function buildFromArray(array $data): Car
    {
        $car = new Car();
        $car->id = $data['id'] ?? null;
        $car->title = $data['title'];
        $car->price = (int)((float)$data['price'] * 100);
        $car->description = $data['description'];
        $car->foto = $data['foto'];
        $car->contacts = $data['contacts'];
        if (!empty($data['tech_info'])) {
            $techInfo = new CarTechInfo();
            $techInfo->model = $data['tech_info']['model'];
            $techInfo->brand = $data['tech_info']['brand'];
            $techInfo->exterior = $data['tech_info']['exterior'];
            $techInfo->mileage = (int) $data['tech_info']['mileage'];
            $techInfo->year = (int) $data['tech_info']['year'];
            $car->techInfo = $techInfo;
        }
        $carParams = new CarParams();
        if (!empty($data['params'])) {
            foreach ($data['params'] as $carParameter) {
                $carParams->add(new CarParameter((int) $carParameter['id'], $carParameter['name']));
            }
        }
        $car->params = $carParams;

        return $car;
    }

    public static function buildFromAR(CarAR $carAR): Car
    {
        $car = new Car();
        $car->id = $carAR->id;
        $car->title = $carAR->title;
        $car->price = $carAR->price;
        $car->description = $carAR->description;
        $car->foto = $carAR->foto;
        $car->contacts = $carAR->contacts;
        if (!empty($carAR->carTechInfo)) {
            $techInfo = new CarTechInfo();
            $techInfo->model = $carAR->carTechInfo->model;
            $techInfo->brand = $carAR->carTechInfo->brand;
            $techInfo->exterior = $carAR->carTechInfo->exterior;
            $techInfo->mileage = $carAR->carTechInfo->mileage;
            $techInfo->year = $carAR->carTechInfo->year;
            $car->techInfo = $techInfo;
        }
        $carParams = new CarParams();
        if (!empty($carAR->params)) {
            foreach ($carAR->params as $carParameter) {
                $carParams->add(new CarParameter($carParameter->id, $carParameter->name));
            }
        }
        $car->params = $carParams;

        return $car;
    }
}
