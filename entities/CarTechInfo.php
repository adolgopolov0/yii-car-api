<?php

declare(strict_types=1);

namespace app\entities;

use yii\helpers\ArrayHelper;

final class CarTechInfo
{
    public string $brand;
    public string $model;
    public string $exterior;
    public int $mileage;
    public int $year;

    public function toArray()
    {
        return ArrayHelper::toArray($this);
    }

}
