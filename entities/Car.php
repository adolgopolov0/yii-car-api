<?php

declare(strict_types=1);

namespace app\entities;

final class Car
{
    public ?int $id;
    public string $title;
    public int $price;
    public string $description;
    public string $foto;
    public string $contacts;
    public ?CarTechInfo $techInfo;
    public CarParams $params;

    public function toArray()
    {
        $list = [
            'id' => $this->id,
            'title' => $this->title,
            'price' => number_format($this->price/100, 2, '.',''),
            'description' => $this->description,
            'foto' => $this->foto,
            'contacts' => $this->contacts,
            'params' => $this->params->toArray(),
        ];
        if ($this->techInfo) {
            $list['tech_info'] = $this->techInfo->toArray();
        }

        return $list;
    }

}
