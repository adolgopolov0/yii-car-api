<?php

declare(strict_types=1);

namespace app\entities;

use yii\helpers\ArrayHelper;

final class CarParams
{
    /** @var CarParameter[] */
    protected $items;

    public function add(CarParameter $carParameter)
    {
        $this->items[] = $carParameter;
    }

    public function toArray()
    {
        return ArrayHelper::toArray($this->items);
    }

    public function notEmpty()
    {
        return !empty($this->items);
    }
}
