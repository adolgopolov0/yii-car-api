<?php

declare(strict_types=1);

namespace app\entities;

final class CarParameter
{
    public int $id;
    public string $name;

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
}
