<?php

declare(strict_types=1);

namespace bootstrap;

use app\repositories\CarRepository;
use app\services\CarService;
use yii\web\Application;

final class ServicesDefinitions implements \yii\base\BootstrapInterface
{
    /**
     * @inheritDoc
     * @var Application $app
     */
    public function bootstrap($app)
    {
        \Yii::$container->set( CarRepository::class);
        \Yii::$container->set( CarService::class,[
            CarRepository::class
        ]);
    }
}
