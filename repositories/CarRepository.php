<?php

declare(strict_types=1);

namespace app\repositories;

use app\entities\Car;
use app\entities\CarParams;
use app\entities\CarTechInfo;
use app\models\Car as CarAR;
use app\models\CarParameter;
use app\models\CarParams as CarParamsAR;
use app\models\CarTechInfo as CarTechInfoAR;
use yii\base\Model;
use yii\db\ActiveRecord;

final class CarRepository
{
    protected $errors = [];

    public function add(Car $car): CarAR
    {
        $carModel = new CarAR();
        $carModel->setAttributes([
            'title' => $car->title,
            'price' => $car->price,
            'foto' => $car->foto,
            'contacts' => $car->contacts,
            'description' => $car->description,
        ]);
        $carModel->save();
        if($car->techInfo) {
            $this->saveCarTechInfo($carModel, $car->techInfo);
        }
        if($car->params->notEmpty()) {
            $this->saveCarParams($carModel, $car->params);
        }
        return $carModel;
    }

    public function edit()
    {
    }

    public function get()
    {
    }

    public function delete()
    {
    }

    public function list()
    {
    }

    public function validate($data):bool
    {
        //todo validateCarData
        $data['price'] = (int)((float)$data['price'] * 100);
        if(! $this->validateAR(new CarAR(), $data)) return false;
        //todo validateTechInfo
        if(!empty($data['tech_info'])){
            if(! $this->validateAR(new CarTechInfoAR(['scenario' => 'create']), $data['tech_info'])) return false;
        }
        //todo validateParams
        if(!empty($data['params'])){
            if(! $this->validateParams($data['params'])) return false;
        }

        return true;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    private function saveCarParams(CarAR $carModel, CarParams $carParams)
    {
        CarParamsAR::deleteAll(['car_id' => $carModel->id]);
        foreach ($carParams->toArray() as $eachParameter){
            $carParamsAR = new CarParamsAR();
            $carParamsAR->car_id = $carModel->id;
            $carParamsAR->param_id = $eachParameter['id'];
        }
    }

    private function saveCarTechInfo(CarAR $carModel, CarTechInfo $techInfo): bool
    {
        if(empty($carTechInfoAR = CarTechInfoAR::findOne(['car_id'=>$carModel->id]))) {
            $carTechInfoAR = new CarTechInfoAR();
        }
        $carTechInfoAR->car_id = $carModel->id;
        $carTechInfoAR->setAttributes([
            'brand' => $techInfo->brand,
            'model' => $techInfo->model,
            'mileage' => $techInfo->mileage,
            'year' => $techInfo->year,
            'exterior' => $techInfo->exterior,
        ]);
        return $carTechInfoAR->save();
    }

    private function validateAR(ActiveRecord $activeRecord, array $data)
    {
        $dataForAR = array_intersect_key($data,
            $activeRecord->attributes);
        $activeRecord->setAttributes($dataForAR);
        if(!$activeRecord->validate()){
            foreach($activeRecord->getErrors() as $field => $errors) {
                $this->errors[$activeRecord->tableName().".".$field] = $errors;
            }

            return false;
        }
        return true;
    }

    private function validateParams(array $data): bool
    {
        foreach ($data as $eachParameter) {
            if(CarParameter::find()->where(['id'=>$eachParameter['id']])->exists()) {
                return false;
            }
        }
        return true;
    }
}
