<?php

declare(strict_types=1);

namespace app\controllers;

use app\entities\CarFactory;
use app\models\Car;
use app\services\CarService;
use Yii;
use yii\base\Exception;
use yii\rest\Controller;
use yii\web\UnprocessableEntityHttpException;

final class CarController extends Controller
{

    public $modelClass = Car::class;

    public $defaultAction = 'list';

    public function actionCreate()
    {
        /** @var CarService $service */
        $service = Yii::createObject([
            'class' => CarService::class,
        ]);
        if($newCar = $service->createCar( Yii::$app->request->post('car'))){
            return [
                'result' => 'ok',
                'data' => $newCar->toArray()
            ];
        } else {
            return [
                'result' => 'error',
                'errors' => $service->getErrors()
            ];
        }

    }

    public function actionUpdate($id)
    {
        return 'action update';
    }

    public function actionDelete()
    {
        return 'action delete';
    }

    public function actionView($id)
    {
        return 'action view';
    }

    public function actionList()
    {
        $data = [];
        foreach (Car::find()->all() as $model) {
            $data[] =CarFactory::buildFromAR($model)->toArray();
        }
        return [
            'result' => 'ok',
            'data' => $data,
        ];
    }

    public function actionSearch()
    {
        return 'action search';
    }


}
