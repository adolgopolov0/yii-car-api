<?php

use yii\db\Migration;

/**
 * Class m230105_095716_create_car_tables
 */
class m230105_095716_create_car_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car', [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->unsigned(),
            'title' => $this->text()->notNull(),// название
            'price' => $this->integer()->notNull(),// цена в копейках
            'foto' => $this->text()->notNull(),// имя файла
            'contacts' => $this->text()->notNull(),// строка контактов
            'description' => $this->text()->notNull(),// описание
        ]);

        $this->createTable('car_tech_info', [
            'id' => $this->primaryKey(),
            'car_id' => $this->bigInteger()->unsigned()->unique()->notNull(),
            'brand' => $this->string()->notNull(),//марка
            'model' => $this->string()->notNull(),//модель
            'exterior' => $this->string()->notNull(),//кузов
            'mileage' => $this->integer()->notNull(),//пробег
            'year' => $this->integer()->notNull(),//год выпуска
        ]);

        $this->addForeignKey(
            'car_tech_info_fk',
            'car_tech_info',
            'car_id',
            'car',
            'id'
        );

        $this->createTable('car_parameter', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique(),
            //'value' => $this->string(),
        ]);

        $this->createTable('car_params', [
            'car_id' => $this->bigInteger()->unsigned(),
            'param_id' => $this->bigInteger()->unsigned(),
        ]);

        $this->addPrimaryKey('car_params_pk', 'car_params', ['car_id', 'param_id']);

        $this->addForeignKey(
            'car_params_car_fk',
            'car_params',
            'car_id',
            'car',
            'id'
        );
        $this->addForeignKey(
            'car_params_parameter_fk',
            'car_params',
            'param_id',
            'car_parameter',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('car_tech_info_fk', 'car_tech_info');
        $this->dropForeignKey('car_params_car_fk', 'car_params');
        $this->dropForeignKey('car_params_parameter_fk', 'car_params');
        $this->dropTable('car_params');
        $this->dropTable('car_parameter');
        $this->dropTable('car_tech_info');
        $this->dropTable('car');
    }

}
