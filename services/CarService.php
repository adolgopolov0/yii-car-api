<?php

declare(strict_types=1);

namespace app\services;

use app\entities\Car;
use app\entities\CarFactory;
use app\repositories\CarRepository;

final class CarService
{
    private CarRepository $carRepository;

    private array $errors = [];

    public function __construct(CarRepository $carRepository)
    {
        $this->carRepository = $carRepository;
    }

    public function createCar(array $carData): ?Car
    {
        if($this->validate($carData)){
            $car = CarFactory::buildFromArray($carData);
            $carAR = $this->carRepository->add($car);
            if(empty($this->errors = $this->carRepository->getErrors())){
               return CarFactory::buildFromAR($carAR);
            }
        }
        return null;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    protected function validate(array $params): bool
    {
        if(!$this->carRepository->validate($params)){
            $this->errors = $this->carRepository->getErrors();
            return false;
        }
        return true;
    }


}
