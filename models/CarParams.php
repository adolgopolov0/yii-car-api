<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_params".
 *
 * @property int $car_id
 * @property int $param_id
 *
 * @property Car $car
 * @property CarParameter $param
 */
class CarParams extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'param_id'], 'required'],
            [['car_id', 'param_id'], 'default', 'value' => null],
            [['car_id', 'param_id'], 'integer'],
            [['car_id', 'param_id'], 'unique', 'targetAttribute' => ['car_id', 'param_id']],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::class, 'targetAttribute' => ['car_id' => 'id']],
            [['param_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarParameter::class, 'targetAttribute' => ['param_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_id' => 'Car ID',
            'param_id' => 'Param ID',
        ];
    }

    /**
     * Gets query for [[Car]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }

    /**
     * Gets query for [[Param]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParam()
    {
        return $this->hasOne(CarParameter::class, ['id' => 'param_id']);
    }
}
