<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $title
 * @property int $price
 * @property string $foto
 * @property string $contacts
 * @property string $description
 *
 * @property CarParams[] $carParams
 * @property CarTechInfo $carTechInfo
 * @property CarParameter[] $params
 */
class Car extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'default', 'value' => null],
            [['price', 'title', 'contacts', 'description', 'foto'], 'required'],
            [['user_id', 'price'], 'integer'],
            [['title', 'contacts', 'description', 'foto'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'title' => 'Title',
            'price' => 'Price',
            'foto' => 'Foto',
            'contacts' => 'Contacts',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[CarParams]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarParams()
    {
        return $this->hasMany(CarParams::class, ['car_id' => 'id']);
    }

    /**
     * Gets query for [[CarTechInfo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCarTechInfo()
    {
        return $this->hasOne(CarTechInfo::class, ['car_id' => 'id']);
    }

    /**
     * Gets query for [[Params]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParams()
    {
        return $this->hasMany(CarParameter::class, ['id' => 'param_id'])->viaTable('car_params', ['car_id' => 'id']);
    }
}
