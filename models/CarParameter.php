<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_parameter".
 *
 * @property int $id
 * @property string|null $name
 *
 * @property CarParams[] $carParams
 * @property Car[] $cars
 */
class CarParameter extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_parameter';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }


    /**
     * Gets query for [[Cars]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::class, ['id' => 'car_id'])->viaTable('car_params', ['param_id' => 'id']);
    }
}
