<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "car_tech_info".
 *
 * @property int $id
 * @property int $car_id
 * @property string $brand
 * @property string $model
 * @property string $exterior
 * @property int $mileage
 * @property int $year
 *
 * @property Car $car
 */
class CarTechInfo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_tech_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id'], 'required','on' => 'default'],
            [['brand', 'model', 'exterior', 'mileage', 'year'], 'required'],
            [['car_id', 'mileage', 'year'], 'integer'],
            [['brand', 'model', 'exterior'], 'string', 'max' => 255],
            [['car_id'], 'unique','on' => 'default'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::class, 'targetAttribute' => ['car_id' => 'id'],'on' => 'default'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => 'Car ID',
            'brand' => 'Brand',
            'model' => 'Model',
            'exterior' => 'Exterior',
            'mileage' => 'Mileage',
            'year' => 'Year',
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'] = $scenarios[self::SCENARIO_DEFAULT];
        return $scenarios;
    }

    /**
     * Gets query for [[Car]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::class, ['id' => 'car_id']);
    }
}
